@extends("layouts.master")


@section("titulo")
	Lista modalidades
@endsection
@section("contenido")
<div class="row">
	@foreach( $modalidades as $modalidad)
		<div class="col-xs-12 col-sm-6 col-md-4" >
			<a href="{{ url('/modalidades/mostrar/' . $modalidad->slug) }}">
				<img src="{{ asset('assets/imagenes/modalidades') }}/{{ $modalidad->imagen}}" class="img-fluid" style="height:100px"/>
				<h5 style="min-height:20px;margin:5px 0 10px 0">{{$modalidad->nombre}}
				</h5>
				{{-- <p>
					{{$modalidad->getParticipantes()}} Participantes.
				</p> --}}
			</a>
		</div>
	@endforeach
</div>
@endsection