@extends("layouts.master")


@section("titulo")
	Lista participantes
@endsection
@section("contenido")
<div class="">
	<div style="clear: right;">
		<h2>
		{{$modalidad->nombre}}
	</h2>
	<h3>
		Familia profesional: {{$modalidad->familiaProfesional}}
	</h3>
	<h3>
		Participantes
	</h3>
	</div>
	<div class="row">
		@foreach( $participantes as $participante)
		<div class="col-xs-12 col-sm-6 col-md-2">
				
				<h5 style="min-height:20px;margin:5px 0 10px 0">{{$participante->nombre}}
				</h5>
				<img src="{{ asset('assets/imagenes/participantes') }}/{{ $participante->imagen}}" class="img-fluid" style="height:100px"/>
		</div>
	@endforeach
	</div>
	
	<div id="botones">
		<a href="{{ url('modalidades/puntuar/ . $modalidad->slug')}}" class="btn btn-success" role="button">Puntuar</a>
		<a href="{{ url('modalidades/resetear/ . $modalidad->slug')}}" class="btn btn-success" role="button">Resetear</a>
	</div>

	<div id="tabla">
		<h3>Resultados</h3>
		<table>
			<tr>
				<th>Nombre</th>
				<th>Puntos</th>
			</tr>
		@foreach ($participantes as $participante)
			<tr>
				<td>{{$participante->nombre}}</td>
				<td>{{$participante->puntos}}</td>
			</tr>
		@endforeach
	</table>
	</div>
</div>
@endsection