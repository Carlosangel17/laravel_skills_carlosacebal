<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class ParticipantesController extends Controller
{
   public function getCrear(){
    	$modalidades = Modalidad::all();
    	return view("participantes.crear", array("modalidades" =>$modalidades));
    }
    public function postCrear(Request $request){
    	$participante = new participante();
    	$participante->nombre = $request->nombre;
    	$participante->apellidos = $request->apellidos;
    	$participante->centro = $request->centro;
    	$participante->tutor = $request->tutor;
    	$participante->fechaNacimiento = $request->fechaNacimiento;
    	$participante->modalidad_id = $request->modalidad;
    	$participante->imagen = $request->imagen->store("", "participantes");
    	try {
    		$participante->save();
    		return redirect("modalidades");
    	} catch (Exception $e) {
    		return redirect("modalidades")->with("mensaje", "Fallo al crear la mascota");
    	}


    }


    public function buscar(){
        $busqueda = $request->busqueda;
            $tutores = Participante::select("tutor")->where("tutor", "like", "%$busqueda%")->pluck("tutor");
            
            return response()->json($tutores);
    }

    public function ganador($slug){
        $modalidad = Modalidad::where("slug", $slug)->first();
        $ganador = Participante::where("modalidad_id", $modalidad_id)->orderBy("puntos")->first();
    }

}
