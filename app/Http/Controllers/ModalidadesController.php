<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class ModalidadesController extends Controller
{
    public function getInicio(){
    	return redirect()->action('ModalidadesController@getIndex');
    }

    public function getIndex(){
    	$modalidades = Modalidad::all();
    	return view("modalidades.index", array("modalidades" =>$modalidades));
    }
    public function getMostrar($slug){
    	$modalidad = Modalidad::where("slug", $slug)->first();
    	$participantes = Participante::where("modalidad_id", $modalidad->id)->get();
    	return view("modalidades.mostrar", array("modalidad" =>$modalidad, "participantes" => $participantes));
    }
    public function puntuar($slug){
    	$modalidad = Modalidad::where("slug", $slug)->first();
    	$participantes = Participante::where("modalidad_id", $modalidad->id)->get();
    	foreach ($participantes as $participante) {
    		$participante->puntos = rand(0, 100);
    		$participante->save();
    	}
    	return redirect("modalidad/mostrar/" . $slug);
    }
    public function resetear($slug){
    	$modalidad = Modalidad::where("slug", $slug)->first();
    	$participantes = Participante::where("modalidad_id", $modalidad->id)->get();
    	foreach ($participantes as $participante) {
    		$participante->puntos = -1;
    		$participante->save();
    	}
    	return redirect("modalidad/mostrar/" . $slug);
    }
    
}
