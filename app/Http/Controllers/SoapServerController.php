<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use App\lib\WSDLDocument;
use SoapServer;

class SoapServerController extends Controller
{
    private $uri = "http://127.0.0.1/Examenes/examen_marzo/laravel_skills_CarlosAcebal/public/api";
	private $clase = "\\App\\Http\\Controllers\\SkillsWebService";
	private $wsdl = "http://127.0.0.1/Examenes/examen_marzo/laravel_skills_CarlosAcebal/public/api/wsdl";

    public function getServer(){
    	
    	$server = new SoapServer($this->wsdl);
    	$server->setClass($this->clase);
    	$server->handle();
    	exit();
    }

    public function getWSDL(){
    	$wsdl = new WSDLDocument($this->clase, $this->uri, $this->uri);
    	$wsdl->formatOutput = true;
		header("Content-Type: text/xml");
		echo $wsdl->saveXML();

    }
}

/**
 * 
 */
class SkillsWebService 
{
	
	public function getNumeroParticipantesCentro(){
		
	}
}
