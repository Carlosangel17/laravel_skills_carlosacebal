<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    Protected $table = "modalidades";

    public function getParticipantes(){
    	return $this->hasMany("app\Participante");
    }
}
